import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;
import java.awt.event.KeyEvent;

public class LetalkaFrame extends JFrame {

    private JPanel panel;
    private JList<Flight> flightJList;
    private DefaultListModel<Flight> flightDefaultListModel;
    private Airport airport;
    private JButton createButton;
    private JButton changeButton;
    private JButton removeButton;
    private JLabel capacityLable;
    private int flightCapacity;

    public LetalkaFrame(){
        panel = new JPanel();
        setContentPane(panel);
        flightJList = new JList<>();
        flightDefaultListModel = new DefaultListModel<>();
        flightJList.setModel(flightDefaultListModel);
        panel.add(flightJList);
        this.airport = AirportFabric.createAirport();
        for(Flight f: airport.getFlights()){
            flightDefaultListModel.addElement(f);
        }
        createButton = new JButton("Создать");
        changeButton = new JButton("Изменить");
        removeButton = new JButton("Удалить");
        flightCapacity = airport.sumFlightCapacity();
        capacityLable = new JLabel("Полная вместимость: " + flightCapacity + " места");
        panel.add(createButton);
        panel.add(changeButton);
        panel.add(removeButton);
        panel.add(capacityLable);


        setSize(new Dimension(750,210));
        setVisible(true);
        panel.registerKeyboardAction(e -> exit(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        createButton.addActionListener(e -> {
            String s = ChooseDialog.choose();
            if (s.compareTo("Вертолет") == 0){
                String name = JOptionPane.showInputDialog("Введите название");
                int capacity = -1;
                int flightAltitude = -1;
                int numberOfBlades = -1;
                try {
                    capacity = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость"));
                    flightAltitude = Integer.parseInt(JOptionPane.showInputDialog("Введите высоту полета"));
                    numberOfBlades = Integer.parseInt(JOptionPane.showInputDialog("Введите количество лопастей"));

                Helicopter helicopter = new Helicopter(capacity, name, flightAltitude,numberOfBlades);
                airport.addFlight(helicopter);
                flightDefaultListModel.addElement(helicopter);
                flightCapacity = airport.sumFlightCapacity();
                capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
                repaint();
                }
                catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(panel, "Тут циферки нужны, плохой ты пользователь!!!");
            }

            }else if (s.compareTo("Пропеллерный самолет") == 0){
                String name = JOptionPane.showInputDialog("Введите название");
                int capacity = -1;
                int fuelReserve = -1;
                int numberOfBlades = -1;
                try {
                    capacity = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость"));
                    fuelReserve = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость бака"));
                    numberOfBlades = Integer.parseInt(JOptionPane.showInputDialog("Введите количество лопастей"));

                PropellerAirbus propellerAirbus = new PropellerAirbus(capacity, name, fuelReserve,numberOfBlades);
                airport.addFlight(propellerAirbus);
                flightDefaultListModel.addElement(propellerAirbus);
                flightCapacity = airport.sumFlightCapacity();
                capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
                repaint();
            }
                catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(panel, "Тут циферки нужны, плохой ты пользователь!!!");
            }
            }else{
                String name = JOptionPane.showInputDialog("Введите название");
                int capacity = -1;
                int fuelReserve = -1;
                int power = -1;
                try {
                    capacity = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость"));
                    fuelReserve = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость бака"));
                    power = Integer.parseInt(JOptionPane.showInputDialog("Введите мощность двигателя"));

                ReactiveAirbus reactiveAirbus = new ReactiveAirbus(capacity, name, fuelReserve, power);
                airport.addFlight(reactiveAirbus);
                flightDefaultListModel.addElement(reactiveAirbus);
                flightCapacity = airport.sumFlightCapacity();
                capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
                repaint();
            }
                catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(panel, "Тут циферки нужны, плохой ты пользователь!!!");
            }
            }
        });

        changeButton.addActionListener(e -> {
            if (flightJList.getSelectedValue() instanceof Helicopter){
                Helicopter helicopter = (Helicopter) flightJList.getSelectedValue();
                String name = JOptionPane.showInputDialog(panel, "Измените название\nЕсли изменение не нужно, нажмите \"Ок\"", helicopter.getName());
                int capacity = -1;
                int flightAltitude = -1;
                int numberOfBlades = -1;
                try {
                    capacity = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость\nЕсли изменение не нужно, нажмите \"Ок\"", helicopter.getCapacity()));
                    flightAltitude = Integer.parseInt(JOptionPane.showInputDialog("Введите высоту полета\nЕсли изменение не нужно, нажмите \"Ок\"", helicopter.getFlightAltitude()));
                    numberOfBlades = Integer.parseInt(JOptionPane.showInputDialog("Введите количество лопастей\nЕсли изменение не нужно, нажмите \"Ок\"",helicopter.getNumberOfBlades()));

                helicopter.setName(name);
                helicopter.setCapacity(capacity);
                helicopter.setFlightAltitude(flightAltitude);
                helicopter.setNumberOfBlades(numberOfBlades);
                flightCapacity = airport.sumFlightCapacity();
                capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
                repaint();
                }
                catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(panel, "Тут циферки нужны, плохой ты пользователь!!!");
            }
            }else if (flightJList.getSelectedValue() instanceof PropellerAirbus) {
                PropellerAirbus propellerAirbus = (PropellerAirbus) flightJList.getSelectedValue();
                String name = JOptionPane.showInputDialog("Введите название\nЕсли изменение не нужно, нажмите \"Ок\"",propellerAirbus.getName());
                int capacity = -1;
                int fuelReserve = -1;
                int numberOfBlades = -1;
                try {
                    capacity = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость\nЕсли изменение не нужно, нажмите \"Ок\"", propellerAirbus.getCapacity()));
                    fuelReserve = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость бака\nЕсли изменение не нужно, нажмите \"Ок\"", propellerAirbus.getFuelReserve()));
                    numberOfBlades = Integer.parseInt(JOptionPane.showInputDialog("Введите количество лопастей\nЕсли изменение не нужно, нажмите \"Ок\"", propellerAirbus.getNumberOfBlades()));

                propellerAirbus.setNumberOfBlades(numberOfBlades);
                propellerAirbus.setName(name);
                propellerAirbus.setFuelReserve(fuelReserve);
                propellerAirbus.setCapacity(capacity);
                flightCapacity = airport.sumFlightCapacity();
                capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
                repaint();
                }
                catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(panel, "Тут циферки нужны, плохой ты пользователь!!!");
            }
            }else{
                ReactiveAirbus reactiveAirbus = (ReactiveAirbus) flightJList.getSelectedValue();
                String name = JOptionPane.showInputDialog("Введите название\nЕсли изменение не нужно, нажмите \"Ок\"", reactiveAirbus.getName());
                int capacity = -1;
                int fuelReserve = -1;
                int power = -1;
                try {
                    capacity = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость\nЕсли изменение не нужно, нажмите \"Ок\"", reactiveAirbus.getCapacity()));
                    fuelReserve = Integer.parseInt(JOptionPane.showInputDialog("Введите вместимость бака\nЕсли изменение не нужно, нажмите \"Ок\"", reactiveAirbus.getFuelReserve()));
                    power = Integer.parseInt(JOptionPane.showInputDialog("Введите мощность двигателя\nЕсли изменение не нужно, нажмите \"Ок\""), reactiveAirbus.getPower());

                reactiveAirbus.setName(name);
                reactiveAirbus.setCapacity(capacity);
                reactiveAirbus.setFuelReserve(fuelReserve);
                reactiveAirbus.setPower(power);
                flightCapacity = airport.sumFlightCapacity();
                capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
                repaint();
                }
                catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(panel, "Тут циферки нужны, плохой ты пользователь!!!");
            }
            }

        });

        removeButton.addActionListener(e -> {
            airport.removeFlight(flightJList.getSelectedValue());
            flightDefaultListModel.removeElement(flightJList.getSelectedValue());
            flightCapacity = airport.sumFlightCapacity();
            capacityLable.setText("Полная вместимость: " + flightCapacity + " места");
            repaint();
        });
    }

    public static void exit(){
        System.exit(0);
    }

    public static void main (String[] args){
        new LetalkaFrame();
    }

}
