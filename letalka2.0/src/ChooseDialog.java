import javax.swing.*;
import java.awt.event.*;

public class ChooseDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList chooseJList;
    private DefaultListModel<String> defaultListModel;

    public ChooseDialog() {
        //contentPane = new JPanel();
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        defaultListModel = new DefaultListModel<>();
        chooseJList.setModel(defaultListModel);
        defaultListModel.addElement("Вертолет");
        defaultListModel.addElement("Пропеллерный самолет");
        defaultListModel.addElement("Реактивный самолет");
        //contentPane.add(chooseJList);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
        setVisible(true);
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static String choose(){
        ChooseDialog cd = new ChooseDialog();
        String s = (String)cd.chooseJList.getSelectedValue();
        return s;
    }
}